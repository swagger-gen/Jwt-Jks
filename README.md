# JWT használata JKS segítségével

JKS létrehozása:
```
keytool -genkeypair -keysize 2048 -keyalg RSA -alias rsakey -keystore keystore.jks -storepass storepass -keypass keypass
```