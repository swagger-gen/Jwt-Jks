package hu.raiffeisen.jwtjks;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.interfaces.RSAKey;
import java.security.interfaces.RSAPublicKey;
import java.util.*;

public class Main {
    private static final String ISSUER = "Raiffeisen Bank";
    private static final String RSA_KEY_ALIAS = "rsakey";

    public static void main(String[] args) throws Exception {
        System.out.println("Start");
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("keystore.jks");

        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(is, "storepass".toCharArray());

        String token = "";
        Key key = keyStore.getKey(RSA_KEY_ALIAS, "keypass".toCharArray());
        if (key instanceof RSAKey) {
            System.out.println("RSAKey");
            RSAKey rsa = (RSAKey) key;
            token = createToken("username", rsa);
            printToken(token);
        }

        Certificate cert = keyStore.getCertificate(RSA_KEY_ALIAS);
        PublicKey publicKey = cert.getPublicKey();

        if (publicKey instanceof RSAPublicKey) {
            System.out.println("RSAPublicKey");
            RSAPublicKey pub = (RSAPublicKey) publicKey;
            validateToken(token, pub);
        }


        System.out.println("End");
    }


    private static String createToken(String userName, RSAKey key) {
        String token = null;
        try {
            Algorithm algorithm = Algorithm.RSA256(key);
            Map<String, Object> header = new HashMap<>();
            header.put("typ", "JWT");

            Date expDate = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(expDate);
            c.add(Calendar.SECOND, 30);
            expDate = c.getTime();

            token = JWT.create()
                    .withArrayClaim("roles", new String[]{"A", "B", "C"})
                    .withHeader(header)
                    .withIssuer(ISSUER)
                    .withSubject(userName)
                    .withIssuedAt(new Date())
                    .withExpiresAt(expDate)
                    .sign(algorithm);
        } catch (JWTCreationException exception) {
            System.out.println(exception.getMessage());
        }
        return token;
    }

    private static void validateToken(String token, RSAPublicKey key) throws Exception {
        Algorithm algorithm = Algorithm.RSA256(key);
        JWTVerifier verifier = com.auth0.jwt.JWT.require(algorithm)
                .withIssuer(ISSUER)
                .withArrayClaim("roles", "B")
                .build(); //Reusable verifier instance
        DecodedJWT jwt = verifier.verify(token);
        System.out.println("Token OK " + jwt.getSubject());
    }

    private static void printToken(String token) {
        System.out.println("Token:" + token);
        String[] tokenParts = token.split("\\.");
        if (tokenParts.length != 3) {
            return;
        }
        byte[] header = Base64.getDecoder().decode(tokenParts[0]);
        byte[] payload = Base64.getDecoder().decode(tokenParts[1]);
        System.out.println("Header:" + new String(header));
        System.out.println("Payload:" + new String(payload));
    }
}
